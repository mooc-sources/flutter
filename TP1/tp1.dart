int add(int x, int y) {
  return x + y;
}

main() {

  // Ex. 1
  int integer = 3;
  String str = "This is a string";
  print("Integer : ${integer} - String : ${str}");

  // Ex. 2
  print("3 + 2 = ${add(3, 2)}");

  // Ex. 3
  var titles = <String>[];
  titles.add("Title 1");
  titles.add("Title 2");
  print("Titles : ${titles}");

  // Ex. 4
  Book b1 = Book("Title 1", "Author 1");
  Book b2 = Book("Title 2", "Author 2");

  var library = <Book>[];
  library.add(b1);
  library.add(b2);

  print(library);

  library.forEach((element) {
    print(element.title);
  });
}

class Book {
  String title = "";
  String author = ""; 

  Book(String title, String author) {
    this.title = title;
    this.author = author;
  }

  @override
  String toString() {
    return title;
  }
}
